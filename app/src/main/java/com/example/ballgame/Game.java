package com.example.ballgame;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.Typeface;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.os.Handler;
import android.view.Display;
import android.view.View;
import android.view.Window;

import java.util.Random;

public class Game extends AppCompatActivity implements SensorEventListener {

	protected int BALL_SIZE = 50;
	protected int SCORE = 0;
	protected int CENTER_X, BALL_X_POS, BALL_X_ACC, BALL_X_VEL, MAX_X = 0;
	protected int CENTER_Y, BALL_Y_POS, BALL_Y_ACC, BALL_Y_VEL, MAX_Y = 0;
	protected boolean SHOW_GAME;
	protected boolean SWITCH_CONTROLS, CHECK_SWITCH_CONTROL = false;
	protected int SWITCH_COUNTER = 0;
	protected String baseColor = "#CD5C5C";
	protected String switchColor = "#4799CC";
	public float frameTime = 0.3333f;
	private SensorManager sensorManager;
	protected MyView gameView;
	protected Random random;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		Point p = new Point();
		random = new Random();
		final Handler h = new Handler();
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_IMMERSIVE
				// Set the content to appear under the system bars so that the
				// content doesn't resize when the system bars hide and show.
				| View.SYSTEM_UI_FLAG_LAYOUT_STABLE
				| View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
				| View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
				// Hide the nav bar and status bar
				| View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
				| View.SYSTEM_UI_FLAG_FULLSCREEN
				| View.KEEP_SCREEN_ON);
//		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams
//		.FLAG_FULLSCREEN);
//		getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
		setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

		Display display = getWindowManager().getDefaultDisplay();
		display.getSize(p);
		MAX_X = p.x;
		MAX_Y = p.y;
		CENTER_X = p.x / 2;
		CENTER_Y = p.y / 2;

		BALL_X_POS = random.nextInt(p.x - 100) + 50;
		BALL_Y_POS = random.nextInt(p.y - 100) + 50;

		sensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
		gameView = new MyView(this);

		setContentView(gameView);

		h.postDelayed(new Runnable() {
			@Override
			public void run() {
				int s = calcMultiply();
				CHECK_SWITCH_CONTROL = SWITCH_CONTROLS;
				if(SCORE > 200 && SWITCH_COUNTER == 0){
					SWITCH_CONTROLS = switchChance();
				}


				if(SWITCH_COUNTER > 50){
					SWITCH_CONTROLS = switchChance();
				}

				if(CHECK_SWITCH_CONTROL != SWITCH_CONTROLS){
					SWITCH_COUNTER = 0;
					String t;
					t = baseColor;
					baseColor = switchColor;
					switchColor = t;
				}
				if(SWITCH_CONTROLS){
					SWITCH_COUNTER++;
				}

				SCORE = s > 0 ? (SCORE + s) : SCORE;

				h.postDelayed(this, 500);
			}
		}, 500);

	}

	@Override
	protected void onResume() {
		super.onResume();
		final Sensor sensorProximity = sensorManager.getSensorList(Sensor.TYPE_PROXIMITY).get(0);
		final Sensor sensorAcc = sensorManager.getSensorList(Sensor.TYPE_ACCELEROMETER).get(0);
		sensorManager.registerListener(this, sensorProximity, SensorManager.SENSOR_DELAY_NORMAL);
		sensorManager.registerListener(this, sensorAcc, SensorManager.SENSOR_DELAY_GAME);

	}

	@SuppressLint("ApplySharedPref")
	@Override
	protected void onPause() {
		super.onPause();
		sensorManager.unregisterListener(this);
		SharedPreferences sharedPreferences = getSharedPreferences("highscorePref", MODE_PRIVATE);
		SharedPreferences.Editor spEditor = sharedPreferences.edit();
		int prev = sharedPreferences.getInt("highscore", 0);
		spEditor.putInt("highscore", Math.max(SCORE, prev));
		spEditor.commit();
	}

	@Override
	public void onSensorChanged(SensorEvent event) {
		if (event.sensor.getType() == Sensor.TYPE_ACCELEROMETER) {
			BALL_X_ACC = (int) event.values[0];
			BALL_Y_ACC = (int) event.values[1];
			updateBall();
		} else if (event.sensor.getType() == Sensor.TYPE_PROXIMITY) {
			SHOW_GAME = (event.values[0] > 2);
		}
	}

	@Override
	public void onAccuracyChanged(Sensor sensor, int accuracy) {
		//pass
	}

	private boolean changeChance() {
		int diff = calcMultiply();
		return random.nextInt(101) > ((95 - diff)-((int)(0.001*SCORE))); //5% chance; in closer to center 6% - 11% chance; additional 1% chance per 1000 score
	}

	private int changeAcc() {

		return (random.nextInt(2) * -1) * random.nextInt(Math.min((int) ((0.1 * SCORE) + 1), ((int) (MAX_X/1.5))));
	}
	private boolean switchChance(){
		return random.nextInt(101) > 50; //constant 2% chance to switch controls
	}

	private void updateBall() {
		if (changeChance()) {
			BALL_X_ACC += changeAcc();
			BALL_Y_ACC += changeAcc();
		}

		BALL_X_VEL += (BALL_X_ACC);
		BALL_Y_VEL += (BALL_Y_ACC);

		float xS = (BALL_X_VEL / 2) * frameTime / 4;
		float yS = (BALL_Y_VEL / 2) * frameTime / 4;
		if (SWITCH_CONTROLS) {
			BALL_X_POS += xS;
			BALL_Y_POS -= yS;
		} else {
			BALL_X_POS -= xS;
			BALL_Y_POS += yS;
		}
		if ((BALL_X_POS <= 0 || BALL_X_POS >= MAX_X) || (BALL_Y_POS <= 0 || BALL_Y_POS >= MAX_Y)) {
			finish();
		}
	}

	private int calcMultiply() {
		int multiply = 0;
		if ((BALL_X_POS < CENTER_X + 25 && BALL_X_POS > CENTER_X - 25) && (BALL_Y_POS < CENTER_Y + 25 && BALL_Y_POS > CENTER_Y - 25)) {
			multiply = 6;
		} else if ((BALL_X_POS < CENTER_X + 125 && BALL_X_POS > CENTER_X - 125) && (BALL_Y_POS < CENTER_Y + 125 && BALL_Y_POS > CENTER_Y - 125)) {
			multiply = 5;
		} else if ((BALL_X_POS < CENTER_X + 225 && BALL_X_POS > CENTER_X - 225) && (BALL_Y_POS < CENTER_Y + 225 && BALL_Y_POS > CENTER_Y - 225)) {
			multiply = 4;
		} else if ((BALL_X_POS < CENTER_X + 325 && BALL_X_POS > CENTER_X - 325) && (BALL_Y_POS < CENTER_Y + 325 && BALL_Y_POS > CENTER_Y - 325)) {
			multiply = 3;
		} else if ((BALL_X_POS < CENTER_X + 425 && BALL_X_POS > CENTER_X - 425) && (BALL_Y_POS < CENTER_Y + 425 && BALL_Y_POS > CENTER_Y - 425)) {
			multiply = 2;
		} else if ((BALL_X_POS < CENTER_X + 525 && BALL_X_POS > CENTER_X - 525) && (BALL_Y_POS < CENTER_Y + 425 && BALL_Y_POS > CENTER_Y - 425)) {
			multiply = 1;
		}
		if (!SHOW_GAME) multiply *= 2;
		return multiply;
	}

	public class MyView extends View {
		Paint paint;
		int x, y;

		public MyView(Context context) {
			super(context);
			paint = new Paint();
		}

		@Override
		protected void onDraw(Canvas canvas) {
			super.onDraw(canvas);
			x = getWidth();
			y = getHeight();
			paint.setTextSize(50);
			Typeface plain = ResourcesCompat.getFont(getContext(), R.font.poiret_one);
			paint.setTypeface(plain);
			if (SHOW_GAME) {
				drawBackground(canvas);
				paint.setStyle(Paint.Style.FILL);
				paint.setColor(Color.parseColor(baseColor));
				canvas.drawCircle(BALL_X_POS, BALL_Y_POS, BALL_SIZE, paint);
			} else {
				drawOverlay(canvas);
			}
			invalidate();
		}

		protected void drawBackground(Canvas canvas) {
			paint.setStyle(Paint.Style.FILL);
			paint.setColor(Color.WHITE);
			canvas.drawPaint(paint);
			paint.setColor(Color.parseColor(baseColor));
			paint.setAlpha(10);
			for (int i = 0; i < 6; i++) {
				canvas.drawCircle(x / 2, y / 2, (BALL_SIZE + 50) + (i * 100), paint);
			}
			paint.setAlpha(255);
			paint.setTextAlign(Paint.Align.CENTER);
			canvas.drawText("score: " + SCORE, x / 2, 80, paint);
//			canvas.drawText("X:" + BALL_X_POS + ", Y:" + BALL_Y_POS, x / 2, y / 2, paint);
		}

		protected void drawOverlay(Canvas canvas) {
			paint.setAlpha(255);
			paint.setColor(Color.BLACK);
			canvas.drawPaint(paint);
			paint.setColor(Color.parseColor(baseColor));
			paint.setAlpha(255);
			paint.setTextAlign(Paint.Align.CENTER);
			paint.setTextSize(100);
			canvas.drawText("score: " + SCORE, (x / 2), y / 2, paint);
		}

	}
}
